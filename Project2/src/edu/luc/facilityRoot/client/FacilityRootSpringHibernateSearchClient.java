package edu.luc.facilityRoot.client;

import org.hibernate.Session;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.luc.facilityRoot.model.eventManager.EventManager;
import edu.luc.facilityRoot.dao.HibernatePGSQLHelper;
import edu.luc.facilityRoot.model.facility.*;
import edu.luc.facilityRoot.model.facilityMaintainance.*;
import edu.luc.facilityRoot.model.facilityUse.*;
import edu.luc.facilityRoot.model.persons.*;
import edu.luc.facilityRoot.*;

//import org.springframework.beans.BeansException;


public class FacilityRootSpringHibernateSearchClient {
	
	public static void main (String args[]) throws Exception {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        System.out.println("***************** Application Context instantiated! ******************");

        //Spring to inject the right object implementation in Inspector for inspector using Setter Injection
        //Also, bootstrapping the Inspector instantiation using factory
        Inspector inspector = (Inspector) context.getBean("Inspector");
	    System.out.println("*************** Creating Inspector service object *************************"); 
	    
	       EventManager em=(EventManager)context.getBean("EventManager");
        //Find an inspector if already exists; if not, create a new one.
        Inspector searchedInspector = em.findInspectorById("XY9999"); 
             
        System.out.println("Searched inspector information .......>>");
        System.out.println("\tName: \t\t\t" + searchedInspector.getFname() + " " + searchedInspector.getLname() + "\n");
         
       Unit inspectAddress = (Townhouse) context.getBean("Townhouse");
           
        System.out.println("\tInspected Address:\t" + inspectAddress.getAddressId() + 
        		"\n\t\t\t\t" + inspectAddress.getStreet() +
        		"\n\t\t\t\t" + inspectAddress.getID() + 
        		"\n\t\t\t\t" + inspectAddress.getCity() + ", " + 
        		inspectAddress.getState() + " " + inspectAddress.getZip() +
        		"\n");
	}
}