package edu.luc.facilityRoot.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import edu.luc.facilityRoot.*;
import edu.luc.facilityRoot.model.eventManager.EventManager;
import edu.luc.facilityRoot.model.facility.*;
import edu.luc.facilityRoot.model.facilityMaintainance.*;
import edu.luc.facilityRoot.model.facilityUse.*;
import edu.luc.facilityRoot.model.persons.*;

import edu.luc.facilityRoot.dao.HibernatePGSQLHelper;
import edu.luc.facilityRoot.dao.InspectorHebirnateDAO;
import edu.luc.facilityRoot.dao.TownhouseHebirnateDAO;


  
  import org.springframework.beans.BeansException;
  


public class FacilityRootSpringHibernateDeleteClient {
	
	
	public static void main (String args[]) throws Exception {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
		System.out.println("***************** Application Context instantiated! ******************");

		//Spring to inject the right object implementation in CustomerService for inspector using Setter Injection
		//Also, bootstrapping the CustomerService instantiation using factory
		Inspector inspector = (Inspector) context.getBean("Inspector");
		System.out.println("*************** Creating Inspector service object *************************"); 
    
	EventManager event_manager=(EventManager)context.getBean("EventManager");
		//Find a customer if already exists; if not, create a new one.
		Inspector searchedInspector = event_manager.findInspectorById("XYZ123"); 
		
		System.out.println("*************** Inspector to be deleted *************************");
		System.out.println("\tName: \t\t\t" + searchedInspector.getFname() + " " + searchedInspector.getLname() + "\n");
        
        Unit inspectTownhouse = (Townhouse)context.getBean("Townhouse");
                System.out.println("\tBilling Address:\t" + inspectTownhouse.getAddressId() + 
        		"\n\t\t\t\t" + inspectTownhouse.getStreet() +
        		"\n\t\t\t\t" + inspectTownhouse.getID() + 
        		"\n\t\t\t\t" + inspectTownhouse.getCity() + ", " + 
        		inspectTownhouse.getState() + " " + inspectTownhouse.getZip() +
        		"\n");
                InspectorHebirnateDAO ih=(InspectorHebirnateDAO)context.getBean("InspectorHebirnateDAO");
                TownhouseHebirnateDAO th=(TownhouseHebirnateDAO)context.getBean("TownhouseHebirnateDAO");
        System.out.println("*************** Inspector to be deleted *************************");
		ih.deleteInspector(searchedInspector);
	     th.deleteTownhouse(inspectTownhouse);
		System.out.println("*************** Inspector deleted *************************");
	}
}
