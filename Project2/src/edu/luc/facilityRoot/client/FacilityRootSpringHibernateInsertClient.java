package edu.luc.facilityRoot.client;

import java.net.URLClassLoader;
import java.util.Arrays;

import org.apache.log4j.chainsaw.Main;
import org.hibernate.Session;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.luc.facilityRoot.model.eventManager.EventManager;
import edu.luc.facilityRoot.dao.HibernatePGSQLHelper;
import edu.luc.facilityRoot.dao.InspectorHebirnateDAO;
import edu.luc.facilityRoot.dao.TownhouseHebirnateDAO;
import edu.luc.facilityRoot.model.facility.*;
import edu.luc.facilityRoot.model.facilityMaintainance.*;
import edu.luc.facilityRoot.model.facilityUse.*;
import edu.luc.facilityRoot.model.persons.*;
import edu.luc.facilityRoot.*;
import org.springframework.beans.BeansException;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class FacilityRootSpringHibernateInsertClient {
	
	public static void main (String args[]) throws Exception {
		
		ApplicationContext context =new /*ClassPathXmlApplicationContext("app-contex.xml");*/FileSystemXmlApplicationContext("/META-INF/app-context.xml");//new ClassPathXmlApplicationContext("META-INF/app-contex.xml"); //new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        System.out.println("***************** Application Context instantiated! ******************");
                
        System.out.println("*************** Instantiating Inspector ***********************");
        Inspector inspector = (Inspector) context.getBean("Inspector");
		inspector.setFname("Berhane");
        inspector.setLname("Zewdie");
        inspector.setId("XYZ123");
        inspector.setStatus("Lead Inspector");
        Unit townhouse=(Townhouse)context.getBean("Townhouse");
        townhouse.setID(inspector.getId());
        townhouse.setAddressId("ADD123");
        townhouse.setStreet("1 Chestnut St.");
        townhouse.setUnit("Suite 506");
        townhouse.setCity("Chicago");
        townhouse.setState("IL");
        townhouse.setZip("60610");	
        
        System.out.println("*************** Saving Inspector ***********************");
        InspectorHebirnateDAO ih=(InspectorHebirnateDAO)context.getBean("InspectorHebirnateDAO");
        TownhouseHebirnateDAO th=(TownhouseHebirnateDAO)context.getBean("TownhouseHebirnateDAO");
        ih.addInspector(inspector);
        th.addTownhouse(townhouse);
        
        System.out.println("*************** Inspector Inserted *************************");
        System.out.println("*************** Townhouse Inserted *************************");
	}
	public static void main2(String[] args) {
        URLClassLoader classLoader = (URLClassLoader)Main.class.getClassLoader();
        System.out.println(Arrays.toString(classLoader.getURLs()));
}
}
