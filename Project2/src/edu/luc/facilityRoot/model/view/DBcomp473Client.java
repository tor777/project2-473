package edu.luc.facilityRoot.model.view;

/*
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.suppor*/

import edu.luc.*;

import edu.luc.facilityRoot.model.facility.Aprtment;
import edu.luc.facilityRoot.model.facility.*;
import edu.luc.facilityRoot.model.*;
import edu.luc.facilityRoot.model.facility.IUnit;
import edu.luc.facilityRoot.model.facility.Office;
import edu.luc.facilityRoot.model.facility.Townhouse;
import edu.luc.facilityRoot.model.facility.Unit;
import edu.luc.facilityRoot.model.facilityMaintainance.*;
import edu.luc.facilityRoot.model.facilityUse.Calculate;
import edu.luc.facilityRoot.model.facilityUse.FUList;
import edu.luc.facilityRoot.model.facilityUse.FacilityUse;
import edu.luc.facilityRoot.model.persons.*;
import edu.luc.facilityRoot.model.facilityUse.*;

import edu.luc.facilityRoot.model.PersonsDAO.PersonsDAO3;

import java.io.IOException;
/*
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.*;
*/

public class DBcomp473Client/* extends HttpServlet*/{
	
	    
			
	    
	    public static void main (String args[]) throws Exception {
				//Server server = new Server(Integer.valueOf(System.getenv("5000")));
		        //ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
		        //context.setContextPath("/");
		        //server.setHandler(context);
		        //context.addServlet(new ServletHolder(new DBcomp473Client()),"/*");
		        //server.start();
		        //server.join();
		        
					System.out.println("Hello, dude!!\n Just started the DBComp473Client.java :))\n\n");
					
				//	ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
			      //  System.out.println("***************** context instantiated! ******************");
			        
			        //BeanFactory factory = context;
			        //System.out.println("***************** factory instantiated! ******************");
			 
			        
			       Person owner=new Owner();
		    		
		    		owner.setStatus("owner");
			    	owner.setFname("Lee");
			    	owner.setLname("Youn");
			    	owner.setId("135");
			        
			        
			    	Person  inspector=new Inspector();
			    		
				    	inspector.setStatus("inspector");
				    	inspector.setFname("Jack");
				    	inspector.setLname("Smith");
				    	inspector.setId("122");
			        
			        
			       Person consumer=new Consumer();
		    		
			    	consumer.setStatus("consumer");
			    	consumer.setFname("Bill");
			    	consumer.setLname("White");
			    	consumer.setId("19");
			    
			/////////////        System.out.println(consumer.getId());
			        //Spring to inject the right object implementation in CustomerService for customer using Setter Injection
			        //Also, bootstrapping the CustomerService instantiation using factory
			      
			        //EXAMPLE of getBean
			        //CustomerService customerService = (CustomerService) factory.getBean("customerService");		
			        
			        
				Facility huge_complex=new Facility();
				
				//Unit &subclasses
				Unit townhous=new Townhouse();
				townhous.setUnit(townhous);
				
				townhous.setID("1");
				townhous.setAddressId("1515");
				townhous.setStreet("Chicago Ave");
				townhous.setCity("Chicago");
				townhous.setZip("60601");
				townhous.setState("IL");
				
				System.out.print("Townhouse info: ");
				townhous.getFacilityInformation();
				
				//Office
				Unit office=new Office();
				office.setUnit(office);
				
				office.setID("2");
				office.setAddressId("5323W");
				office.setStreet("North St");
				office.setCity("Milwaukee");
				office.setZip("60601");
				office.setState("WI");
				System.out.print("Office info: ");
				
				office.getFacilityInformation();
				
				//Apartment
				Unit apartment=new Aprtment();
				apartment.setUnit(apartment);
				
				apartment.setID("3");
				apartment.setAddressId("2323");
				apartment.setStreet("Roosevelt St");
				apartment.setCity("Chicago");
				apartment.setZip("60601");
				apartment.setState("IL");
				apartment.addFacilityDetails();
				System.out.print("Appartment info: ");
				apartment.getFacilityInformation();
				
				
				//Facility
				huge_complex.addNewFacility(apartment);
				huge_complex.addNewFacility(townhous);
				huge_complex.addNewFacility(office);
				
				System.out.println("\nOur Facility has: ");
				huge_complex.listFacilities();
				
					//FacilityMainainence			
				
				//Facility Maint List
				FMList fml=new FMList();
				
				System.out.println("\n Facility Maintenance LIST:\n ");
				fml.addEntryToFacilityMaintList("\nPlumber: fix water leak at Unit 1 \n");
				fml.addEntryToFacilityMaintList("Housekeeper: clean up the front windows at Unit 1\n\n");
				fml.addEntryToFacilityMaintList("Security: Check parking lot for unauthorized vehicles\n");
				fml.listMainRequest();
			
				
				String entry_completed_job="\nElectrician: change broken switch at Unit 2\n";
				
				//Owner uses the uses Info List
					Person owner1=new Owner();
					owner1.addEntryToFacilityMaintList(entry_completed_job);
					owner1.addEntryToCompletedJobList(entry_completed_job);
					fml.listMaintenance();
			
					
				//Facility Maintenace
					
					//
					IFacilityMaintainance fm =new FacilityMaintainance();
					
					fm.getMaintainanceInfo();
					fm.makeFacilityMaintainaceRequest();
					fm.scheduleMaintenance(1);
					
								
				FMCalculate fmc=new FMCalculate();
			
				//down_time_for_facility
				fmc.calcDownTimeForFacility(5);  //5 is a maintenance time (hours)
				
				//problem_Rate_for_facility
				fmc.calcProblemRateForFacility(5); //2 is # of problem per day
				
				//maintenance_cost_for_facility
				fmc.calcMaintenanceCostForFacility(90,4, 500);/// user should provide this info:  labor_cost, hours_worked, parts_cost
				
				//
				
				//Calculate the valuew to be inserted into DB tables
				//fm_problem_name, problem_Rate_for_facility, maintenance_cost_for_facility, down_time_for_facility
				
				//
				
				//FacilityUse
				FacilityUse fu = new FacilityUse();
				fu.assignedFacilityToUse(0);
				fu.getUseInfo();
				fu.vacateFacility(0);
				fu.isInUse();
				           //FacilityUse's FUList//  needs to add dependencies
				FUList ful=new FUList();
				fu.vacateFacility(1);
				fu.IsInUseDuringInterval(0);//true/false or 0/1
				
				ful.listActualUsage();
				ful.listInspection();
				        //   ---//----
				Calculate c=new Calculate();
		    	System.out.printf("Usage Rate: %d\n\n",(c.calcUsageRate(3)));
			
		    	//Person person=new Person();
	
		    	
		    	
		    	///////////////////////////////////////
		    	////////////////////////////////////This is for PersonDAO.java ------ignore----
		    	/////////////////////////////
		  /*  	PersonsDAO pdao22=new PersonsDAO();
		    	
		    	String person_status22="owner";              //DB table: Owner
			   	
			    	pdao22.getPerson(person_status22);
			    	
			    	String person_status23="consumer";         //DB Person: Consumer
				   	
				    	pdao22.getPerson(person_status23);	
				    	
				    	String person_status24="inspector";      // DB Peson: Inspector
					   	
					    	pdao22.getPerson(person_status24);
				    	  
				////////////////////////////////////////////
				    	/////////////////////////////
				    	//////////////////////////
		    	//adds Person objects to the DB as three separate tables
		    */
					    	PersonsDAO3 pdao=new PersonsDAO3();
		    			    		    	
		        //	Person owner=new Owner();		    		    			    	    	
		    	    String person_status="owner";              //DB table: Owner
		    	   
		    	    pdao.createPersonsDBtables(person_status);
			     	pdao.addPERSONStoDB(owner, person_status);
			    	pdao.getPersonFromDB(owner.getId(), person_status, owner);
			    	
			    //	Person consumer=new Consumer();	    	
			    	String person_status2="consumer";         //DB table: Consumer
			    	   pdao.createPersonsDBtables(person_status2);
			   	    pdao.addPERSONStoDB(consumer, person_status2);
			    	pdao.getPersonFromDB(consumer.getId(), person_status2, consumer);	
			    	
			    	//Person inspector=new Inspector();
			        	String person_status3="inspector";      // DB table: Inspector
			    	    pdao.createPersonsDBtables(person_status3);
			        	pdao.addPERSONStoDB(inspector, person_status3);
			    	    pdao.getPersonFromDB(inspector.getId(), person_status3, inspector);
		    	
			    
			    	///Create the Apartment table
			    String a="apartment";
		    	pdao.createFacilityUnitDBtables("apartment");
		    	pdao.addUNITStoDB(apartment, a);
		    	
		    	
		    	//Town-house table
		    	String t="townhouse";
		    	pdao.createFacilityUnitDBtables("townhouse");
		    	pdao.addUNITStoDB(townhous,t);
		    	
		    	//Office table
		    	String o="office";
		    	pdao.createFacilityUnitDBtables("office");
		    	pdao.addUNITStoDB(office, o);
		    	
		    	////////////Facility Maintenance tables///////////////////////////////////
		    	
		    	    //FMaintRequestImpl table
		    	FMaintenanceRequestImpl fmri=new FMaintenanceRequestImpl();
		    	
		    	fmri.setID("1");
		    	fmri.setMaintenance_request_type("non-emergency");
		    	fmri.setFmRequestCompleteOrNot("pending");
		    	
		    	pdao.createFMaintenaceRequestImplDBtables();
		    	pdao.addEntryIntoFMaintanenceRequestImplDBtable(fmri);
		    	
		    	///FMaintCalcimpl table
		    	FMaintenanceCalcImpl fmric=new FMaintenanceCalcImpl();
		    //set values for FmaintanenceCalcImpl DB table: id,fm_problem_name, problem_Rate_for_facility, maintenance_cost_for_facility, down_time_for_facility

		    	fmric.setID("2");                             //
		    	fmric.setFmProblemName("water leak");        //
		    	fmric.setProblemRateForFacility("urgent");  //
		    	fmric.setMaintCostForFacility("1530");     //		    
		    	fmric.setDownTimeForFacility("2");
		    	fmric.setNumberOfMaintRequest("1");    //
		    	fmric.setMaintenanceTimeHours("3");   //
		    	fmric.setPartsCost("335");           //
		    	fmric.setLaborCost("90");           //
		    	fmric.setHoursWorked("2");         //
		    	                                   
		    	pdao.createFMaintenanceCalcImplDBtables();
		    	pdao.addEntryIntoFMaintenaceCalcImplDBtable(fmric);
		    	
		    			    	
	//Facility Use table   attributes: id,is_in_use, assigned_to_use, is_vacated , facility_inspection_name, usage_rate;
		    	FacilityUse fu2=new FacilityUse();
		    	fu2.setID("1");
		    	fu2.setIsInUse("in-use");
		    	fu2.setAssignedToUse("assigned-to-use");
		    	fu2.setIsVacated("not-vacated");
		    	fu2.setVacilityInspectionName("townhouse");
		    	fu2.setUsageRate("100%");
		    	   
               // System.out.println("\nTEST OF set() method for FUse object:   ID:" + fu.getID()+" "+fu.getIsInUse()+" "+ fu.getAssignedToUse()+" "+fu.getIsVacated()+" "+fu.getFacilityInspectionName()+" "+fu.getUsageRate()+"\n\n");
                
		    	pdao.createFUseListAndCalcDBtables();
		    	pdao.addEntryIntoFUlistandCalcDBtables(fu2);
		    	

		
		    	
		    			    	System.out.println("\nDB entry/call has been completed!! Check your DB for data entry!!!\n" +
		    			    			"" +
		    			    			"\nPost-pdao test-string!!");

			}
			}
