package edu.luc.facilityRoot.model.facilityMaintainance;

import java.util.ArrayList;

import edu.luc.facilityRoot.model.facility.Facility;

public class FMList extends Facility implements IFMList{

	public ArrayList<String> facility_maint_list=new ArrayList<String>();
	public ArrayList<String> performed_facility_maint_jobs=new ArrayList<String>();
	
	
	public void addEntryToFacilityMaintList(String fml_req_entry){
		
		facility_maint_list.add(fml_req_entry);
		
	}
	
	public void addEntryToCompletedMaintanceJob(String completed_m_job){
		
		performed_facility_maint_jobs.add(completed_m_job);
	}
	
	
	public void listMainRequest() {
		
		if(!facility_maint_list.isEmpty()){
		for(int i=0; i<facility_maint_list.size();i++){
			
		System.out.println(facility_maint_list.get(i));
		}
		}else{
			System.out.println("There are no facility maintance requests in the list!!!");
		}
		}
		
		
	

	public void listMaintenance() {//performed maintenance jobs
	//
		System.out.println("\nCompleted job:  ");
		if(!performed_facility_maint_jobs.isEmpty()){
	
		for(int i=0; i<performed_facility_maint_jobs.size();i++){
			
			System.out.println(performed_facility_maint_jobs.get(i));
			}
		}
		else{
			System.out.println("There are no maintance job(performed) in the list!!!");}
	}

}
