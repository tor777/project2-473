package edu.luc.facilityRoot.model.facilityMaintainance;

public class FMCalculate  implements IFMCalculate{

	public float calcMaintenanceCostForFacility(int labor_cost,int hours_worked, int parts_cost) {
		
		float sale_tax=(float)0.045;
		if(labor_cost!=0&&parts_cost!=0){
		float total_cost=(labor_cost*hours_worked+parts_cost)*sale_tax;
		System.out.println("\nTotal Maint.Cost: $"+total_cost+"\n");
		}
		return 0;
	}
	
	public float calcProblemRateForFacility(int number_of_maint_reqsts){
		
		float rateproblem;
		if(number_of_maint_reqsts!=0){
		rateproblem=number_of_maint_reqsts/30;
		System.out.println("\nProblem RATE: "+ rateproblem+"\n");
		}
		return 0;
		
	}
	
	public int calcDownTimeForFacility(int maintenance_time_hours){
		
		int downTime=maintenance_time_hours;
		System.out.println("Down Time: "+downTime+" hours");
		return downTime;
	}
	}


