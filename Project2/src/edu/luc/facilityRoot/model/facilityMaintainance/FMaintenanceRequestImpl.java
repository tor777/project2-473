package edu.luc.facilityRoot.model.facilityMaintainance;

import edu.luc.facilityRoot.model.facility.Unit;

public class FMaintenanceRequestImpl extends FMList {

	
//	public FMaintenanceRequestImpl(){}
	String ID;
	String maintenance_request_type;  //urgent, non-emergency
	String fm_request_complete_or_not;
	
	public void setID(String ID){
		 this.ID=ID;
		}
	
	public String getID() {
		return ID;
	}
	
	public void setMaintenance_request_type(String maintenance_request_type){
		
	this.maintenance_request_type=maintenance_request_type;	
	
	}
	
	public String getMaintenanceRequestType(){
		
	return maintenance_request_type;	
	}
	
	
	public void setFmRequestCompleteOrNot(String fm_request_complete_or_not){
		
		this.fm_request_complete_or_not=fm_request_complete_or_not;
			
	}
	
	public String getFmRequestCompleteOrNot(){
		
		return fm_request_complete_or_not;
	}
	
	
}
