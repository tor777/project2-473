package edu.luc.facilityRoot.model.facilityMaintainance;

public class FMaintenanceCalcImpl extends FMCalculate{

	String labor_cost,hours_worked, parts_cost, number_of_maint_reqsts, maintenance_time_hours;
	
	String ID, fm_problem_name;
	String problem_Rate_for_facility, maintenance_cost_for_facility, down_time_for_facility; 

public void setLaborCost(String labor_cost){
	
	this.labor_cost=labor_cost;
}

public String getLaborCost(){
	return labor_cost;
	
}

public void setHoursWorked(String hours_worked){
	
this.hours_worked=hours_worked;
}


public String getHoursWorked(){
	
return hours_worked;	
}


public void setPartsCost(String parts_cost){
	
	this.parts_cost=parts_cost;
	}

public String getPartsCost(){
return parts_cost;	
}

public void setNumberOfMaintRequest(String number_of_maint_reqsts){
this.number_of_maint_reqsts=number_of_maint_reqsts;	
}

public String getNumberOfMaintRequest(){
return number_of_maint_reqsts;	
}

public void setMaintenanceTimeHours(String maintenance_time_hours){
this.maintenance_time_hours=maintenance_time_hours;	
}

public String getMaintTimeHours(){
	
return maintenance_time_hours;	
}


public void setID(String ID){
	 this.ID=ID;
	}

public String getID() {
	return ID;
}

//fm_problem_name, problem_Rate_for_facility, maintenance_cost_for_facility, down_time_for_facility; 

public void setFmProblemName(String fm_problem_name){
	
this.fm_problem_name=fm_problem_name;	
}

public String getFmProblemName(){
return fm_problem_name;	
}

public void setProblemRateForFacility(String problem_Rate_for_facility){
	this.problem_Rate_for_facility=problem_Rate_for_facility;
}

public String getProblemRateForFacility(){
return problem_Rate_for_facility;	
}

public void setMaintCostForFacility(String maintenance_cost_for_facility){
	this.maintenance_cost_for_facility=maintenance_cost_for_facility;
}

public String getMaintCostForFasility(){
return maintenance_cost_for_facility;	
}

public void setDownTimeForFacility(String down_time_for_facility){
this.down_time_for_facility=down_time_for_facility;	
}

public String getDownTimeForFacility(){
return down_time_for_facility;	
}

}
