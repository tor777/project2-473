package edu.luc.facilityRoot.model.facilityMaintainance;

public interface IFMCalculate {
	
 float calcMaintenanceCostForFacility(int labor_cost,int hours_worked, int parts_cost);
	

}
