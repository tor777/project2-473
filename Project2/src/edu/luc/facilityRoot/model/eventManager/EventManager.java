
package edu.luc.facilityRoot.model.eventManager;
import edu.luc.facilityRoot.*;
import edu.luc.facilityRoot.model.facility.*;
import edu.luc.facilityRoot.model.persons.Inspector;
import edu.luc.facilityRoot.dao.FMaintenanceRequestImplHibernateDAO;
import edu.luc.facilityRoot.dao.InspectorHebirnateDAO;
import edu.luc.facilityRoot.dao.TownhouseHebirnateDAO;

public class EventManager {
	//private Inspector inspector;
	
	private InspectorHebirnateDAO custDAO=new InspectorHebirnateDAO();
	private TownhouseHebirnateDAO custDAO2=new TownhouseHebirnateDAO();
	private Unit townhouse=new Townhouse();
	private FMaintenanceRequestImplHibernateDAO custDAO3 = new FMaintenanceRequestImplHibernateDAO();
	
		//SEARCH (RETRIEVE/SELECT) inspector by ID from the DB
	public Inspector findInspectorById(String ID) {
			
		try {
			Inspector inspector = custDAO.retrieveInspector(ID);
	    	return inspector;
	    } catch (Exception se) {
	      System.err.println("EventManager: Threw an Exception retrieving inspector.");
	      System.err.println(se.getMessage());
	    }
		return null;
	}
	
	//INSERT a new inspector in the DB
	public void addInspector(Inspector inspector) {
		
		try {
			custDAO.addInspector(inspector);
	    } catch (Exception se) {
	      System.err.println("EventManager: Threw an Exception retrieving/adding inspector");
	      System.err.println(se.getMessage());
	    }
	}
	
	//DELETE an  inspector from DB
	public void deleteInspector(Inspector inspector) {
			
			try {
				custDAO.deleteInspector(inspector);
		    } catch (Exception se) {
		      System.err.println("EventManager: Threw an Exception deleting inspector.");
		      System.err.println(se.getMessage());
		    }
	}
	
	//search townhouse address by ID from the DB
	public Unit findInspectedAddress(String ID) {
				
			try {
			Unit townhouse = custDAO2.retrieveTownhouse(ID);
		    	return townhouse;
		    } catch (Exception se) {
		      System.err.println("EventManager: Threw an Exception retrieving inspector.");
		      System.err.println(se.getMessage());
		    }
			return null;
		}
	
}
