package edu.luc.facilityRoot.model.facility;

	import java.util.*;
import java.sql.*;
import java.io.*;

	public interface IFacility {

		public void listFacilities () ;
		public  String getFacilityInformation();
		public  void requestAvailableCapacity();
		public  void removeFacility(Unit unit);
		void addNewFacility(Unit unit);
		void addFacilityDetail(Unit unit);
	}

	

