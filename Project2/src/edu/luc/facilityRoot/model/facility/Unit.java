package edu.luc.facilityRoot.model.facility;

import java.util.ArrayList;

public class Unit extends Facility implements IUnit{
	private String ID;
	private String addressId;
	private String street;
	protected Unit unit;
	private String city;
	private String state;
	private String zip;
	public ArrayList<String>unit_info=new ArrayList<String>();
	private String string;
	
	
	public void setUnit(Unit unit){
		 this.unit=unit;
		}
	
	Unit getUnit(Unit unit) {
		return unit;
	}
	
	public void addFacilityDetails() {
		this.setID(ID);
		this.setAddressId(addressId);
		this.setStreet(street);
		this.setCity(city);
		this.setState(state);
		this.setZip(zip);
		
	}
	
		
		
		@Override
		public String getFacilityInformation() {
			//run db query
			ID=getID();
			addressId=getAddressId();
			street=getStreet();
			city=getCity();
			state=getState();
			zip=getZip();
			
			unit_info.add(ID);
			unit_info.add(addressId);
			unit_info.add(street);
			unit_info.add(city);
			unit_info.add(state);
			unit_info.add(zip);
			System.out.print("Facility Info:\n");
			for(int i=0;i<unit_info.size();i++){
			
			System.out.print(unit_info.get(i)+" ");
			}
			System.out.print("\n\n");
			return null;
			
		}

		
		@Override
		public void addNewFacility(Unit unit) {
			facility.add(unit);
			
			//run db query to add 
		}

		@Override
		public void addFacilityDetail(Unit unit) {
			unit.setID(ID);
			unit.setAddressId(addressId);
			unit.setStreet(street);
			unit.setCity(city);
			unit.setState(state);
			unit.setZip(zip);
			
		}

		@Override
		public void removeFacility(Unit unit) {
			
			facility.remove(unit);
		}
		
		
		public void setID(String ID) {
			// TODO Auto-generated method stub
			this.ID=ID;
		}
		
		public String getID() {
			// TODO Auto-generated method stub
			return ID;
		}
		
		public String getAddressId() {
			return addressId;
		}
		
		public void setAddressId(String addressId) {
			this.addressId = addressId;
		}
		
		public String getStreet() {
			return street;
		}
		public void setStreet(String street) {
			this.street = street;
		}
		
		
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getZip() {
			return zip;
		}
		public void setZip(String zip) {
			this.zip = zip;
		}

		public void setUnit(String string) {
		
			this.string=string;
		}

		
	
}
