package edu.luc.facilityRoot.model.facilityUse;

import java.util.*;
import java.util.*;
import java.sql.*;
import java.io.*;

import edu.luc.facilityRoot.model.facility.Facility;

public class FacilityUse extends Facility implements IFacilityUse{

	int IsInUse;
	int assignedToUse;
	int vacated; 
	float usageRate=(float) 0.00;
	 int useinfo;      //40% ..etc 
	public String ID;   //like list ID
	int zero_or_one;
	int true_or_false;
	
	
	public String is_in_use, assigned_to_use, is_vacated , facility_inspection_name, usage_rate;
	
	
	public void setID(String ID){
		 this.ID=ID;
		}

	public String getID() {
		return ID;
	}
public boolean isInUse() {
	// TODO Auto-generated method stub
	return false;
}
/////////////////////////////set & get/////////////////////////
public void setIsInUse(String is_in_use){
	
this.is_in_use=is_in_use;
}

public String getIsInUse(){
return is_in_use;	

}

public void setAssignedToUse(String assigned_to_use){
	this.assigned_to_use=assigned_to_use;
}

public String getAssignedToUse(){
return assigned_to_use;	
}

public void setIsVacated(String is_vacated){
	this.is_vacated=is_vacated;
}

public String getIsVacated(){
return is_vacated;
}

public void setVacilityInspectionName(String facility_inspection_name){
	this.facility_inspection_name=facility_inspection_name;
	
}

public String getFacilityInspectionName(){
	return facility_inspection_name;
	
}

public void setUsageRate(String usage_rate){
this.usage_rate=usage_rate;	

}

public String getUsageRate(){
return usage_rate;	
}



public void setUseInfo(int useinfo){//create table useinfo(ID int, useinfo int, assignedFacilitytoUse int, vacateFacility int, usagerate float)
	this.useinfo=useinfo;
}


public int getUseInfo(){
		return useinfo;
}



public int getAssignedFacilityToUse(){
	return assignedToUse;
		
}

public void assignedFacilityToUse(int true_or_false)
{
this.true_or_false=true_or_false;	
}

public void vacateFacility(int vacated){
this.vacated=vacated;	
}

public int getvacatedFacility(){
	
return vacated;
}

public void IsInUseDuringInterval(int IsInUse){
	
this.IsInUse=IsInUse;  //true==1; false==0
}


public int IsInUseDuringInterval(){
	return IsInUse;
}

}




