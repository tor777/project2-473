package edu.luc.facilityRoot.model.persons;

public interface IPerson {
	public String getId(); 
	public void setId(int id); 
	
	public String getFname() ;
	public void setFname(String fname) ;
	
	public String getLname() ;
	public void setLname(String lname);
	
	public String getAdress();
	public void setAdress(String adress) ;
	
	public String getStatus() ;
	public void setStatus(String status) ;
}
