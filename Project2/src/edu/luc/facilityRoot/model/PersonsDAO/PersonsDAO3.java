package edu.luc.facilityRoot.model.PersonsDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//import luc.edu.view.*;
import edu.luc.facilityRoot.model.persons.*;

import edu.luc.facilityRoot.model.PersonsDAO.DBHelper;
import edu.luc.facilityRoot.model.facilityUse.*;
import edu.luc.facilityRoot.model.facilityMaintainance.*;
import edu.luc.facilityRoot.model.facility.Facility;
import edu.luc.facilityRoot.model.facility.Unit;
import edu.luc.facilityRoot.model.persons.*;
import edu.luc.facilityRoot.model.view.DBcomp473Client;

public class PersonsDAO3 extends Person {

	public PersonsDAO3(){}
	
	

	
	public void createPersonsDBtables(String person_status){//OK
			// Connection con=DBHelper.getConnection();
    	    	 	
    	
    ////Creates the Consumer, Owner, and Inspector tables in your PostgresSQL database    :))   IT WORKS!! !!
		
	    try {
	    	
	        	
	    	if(person_status.equals("inspector")){
	    		Statement st1=DBHelper.getConnection().createStatement();
	    	String createTablePerson="CREATE TABLE IF NOT EXISTS inspector(status varchar(32), fname varchar(32), lname varchar(32), id varchar(32))";
	    	int custRS1 = st1.executeUpdate(createTablePerson);
	    
	    	System.out.println("\nPersonDAO3: *************** Query " + createTablePerson);
	    	//custRS1.close();   	
        	 //st1.close();
	    	}
	    	if(person_status.equals("owner")){
	    		Statement st1=DBHelper.getConnection().createStatement();	    	
		    	String createTablePerson2="CREATE TABLE IF NOT EXISTS owner(status varchar(32), fname varchar(32), lname varchar(32), id varchar(32))";
		    	int custRS2 = st1.executeUpdate(createTablePerson2);
	    		                    	      //close to manage resources
	        	      //custRS2.close();	
		    		st1.close();
	    	}
	    	
	    	if(person_status.equals("consumer")){
	    		Statement st1=DBHelper.getConnection().createStatement();
	    	String createTablePerson3="CREATE TABLE IF NOT EXISTS consumer(status varchar(32), fname varchar(32), lname varchar(32), id varchar(32))";
	    	int custRS3 = st1.executeUpdate(createTablePerson3);//executeUpdate(createTablePerson3);
	    	System.out.println("\nPersonDAO3: *************** Query " + createTablePerson3);
	    	
       	    //  custRS3.close();
             	st1.close();     
	    	}
	    	
	    //	if (person_status!="owner"||person_status!="consumer"||person_status!="inspector"){
	    	//	    System.out.println("Error!!!! Please provide the proper person status(owner, consumer, or inspector");
	   	    		//    	}    
	    	//con.close();
            	    	    }
	    catch (SQLException sql_e) {
	      System.err.println("PersonDAO3-create-PDB-tables: Threw a SQLException retrieving the object===PDAO3.");
	      System.err.println(sql_e.getMessage());
	      sql_e.printStackTrace();
	    }
	    
	}

			

	public void addPERSONStoDB(Person pers, String person_status){
			
	//	Person o=new Owner();
	//	Person c=new Consumer();
	//	Person i=new Inspector();
		
		Connection con = DBHelper.getConnection();
        PreparedStatement personPst = null;
       // PreparedStatement personPst2 = null;
      //  PreparedStatement personPst3 = null;

        try {
        	
        	//Insert the Owner object	
        	  if (person_status=="owner"){
           	   
        		  String personStm = " INSERT INTO owner(status,fname,lname,id) VALUES(?,?,?,?)";
                  personPst = con.prepareStatement(personStm);
                  personPst.setString(1, pers.getStatus());
                  personPst.setString(2, pers.getFname());       
                  personPst.setString(3, pers.getLname());
                  personPst.setString(4,pers.getId());
                  personPst.executeUpdate();
                  
                  System.out.println("\nTEST STRING ____>Add DB Entry:::::Person status: " + pers.getStatus()+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
                  
             }	
        	//Insert the Consumer object
                  else if (person_status=="consumer"){//(status, fname, lname, id)
    	   
    	   String personStm2 = " INSERT INTO consumer(status,fname,lname,id) VALUES(?,?,?,?)";
           personPst = con.prepareStatement(personStm2);
           personPst.setString(1, pers.getStatus());
           personPst.setString(2, pers.getFname());       
           personPst.setString(3, pers.getLname());
           personPst.setString(4,  pers.getId());
           personPst.executeUpdate();  
           
           System.out.println("\nTEST STRING _2 ____>Add DB Entry::::"+ pers.getStatus()+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
       }
       //Insert the Inspector object
                  else if (person_status=="inspector"){
    	       	
    	   String personStm3 = " INSERT INTO inspector(status,fname,lname,id) VALUES(?,?,?,?)";
           personPst = con.prepareStatement(personStm3);
           personPst.setString(1, pers.getStatus());
           personPst.setString(2, pers.getFname());       
           personPst.setString(3, pers.getLname());
           personPst.setString(4, pers.getId());
           personPst.executeUpdate();   
           
           System.out.println("\nTEST STRING __3__>Add DB Entry::::"+ pers.getStatus()+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
       }
                   
        } catch (SQLException ex) {

        } finally {

            try {
                if (personPst != null) {
                //	addPst.close();
                	personPst.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
      	      System.err.println("PersonsDAO3-AddPtoDB: Threw a SQLException saving the person object.");
    	      System.err.println(ex.getMessage());
            }
        }
		
	}
	
	
	
	
	public Person getPersonFromDB(String id, String person_status, Person pers){
		Connection con = DBHelper.getConnection();
		
		DBcomp473Client getInfoFromClientSet=new DBcomp473Client();
		
		try { 	
			
			if(person_status.equals("owner")){
				///Person pers=new Owner();
	    	//Get Owner
	    	Statement st = DBHelper.getConnection().createStatement();
	    	String selectCustomerQuery = "SELECT id, lname, fname, status FROM owner WHERE id = '" + id + "'";

	    	ResultSet custRS = st.executeQuery(selectCustomerQuery);      
	    	System.out.println("PersonsDAO3: *************** Query " + selectCustomerQuery);
		      
	    // 	  System.out.println("\nSELECTE from DB \nPerson status: " + pers.getStatus()+"\nID: "+pers.getId()+"\nName: "+ pers.getFname() +" "+pers.getLname()+ "\n\n");
		
	      //Get Owner
		  Person owner = new Owner();
	      while ( custRS.next() ) {
	    	  owner.setId(custRS.getString("id"));
	    	  owner.setLname(custRS.getString("lname"));
	    	  owner.setFname(custRS.getString("fname"));
	    	  owner.setStatus("status");
	      }
	      //close to manage resources
	      custRS.close();
	      	st.close();
			}
			
			if(person_status.equals("inspector")){
	      //Get Inspector
	      Statement st2 = DBHelper.getConnection().createStatement();
	  	String selectCustomerQuery2 = "SELECT id, lname, fname, status FROM inspector WHERE id = '" + id + "'";

	  	ResultSet custRS2 = st2.executeQuery(selectCustomerQuery2);      
	  	System.out.println("PersonsDAO3: *************** Query " + selectCustomerQuery2);
	  	
	    //Get Inspector
		  Person inspector = new Inspector();
	    while ( custRS2.next() ) {
	  	  inspector.setId(custRS2.getString("id"));
	  	  inspector.setLname(custRS2.getString("lname"));
	  	  inspector.setFname(custRS2.getString("fname"));
	  	  inspector.setStatus("status");
	    }
	    //close to manage resources
	    custRS2.close();
	    st2.close();
	      
	      return inspector;
	    }
			
			
		if(person_status.equals("consumer")){
			   //Get Consumer
			
		      Statement st3 = DBHelper.getConnection().createStatement();
		  	String selectCustomerQuery3 = "SELECT id, lname, fname, status FROM consumer WHERE id = '" + id + "'";

		  	ResultSet custRS3 = st3.executeQuery(selectCustomerQuery3);      
		  	System.out.println("CustomerDAO: *************** Query " + selectCustomerQuery3);
		  	
		    //Get Consumer 
		  	Person consumer = new Consumer();
		    while ( custRS3.next() ) {
		  	  consumer.setId(custRS3.getString("id"));
		  	consumer.setLname(custRS3.getString("lname"));
		  	  consumer.setFname(custRS3.getString("fname"));
		  	  consumer.setStatus("status");
		    }
		    //close to manage resources
		    custRS3.close();
		    st3.close();
		return consumer;
		}
		}
	    catch (SQLException se) {
	      System.err.println("PersonsDAO-getPersonfDB: Threw a SQLException retrieving the customer object.");
	      System.err.println(se.getMessage());
	      se.printStackTrace();
	    }
	    
	  	try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	public void createFacilityUnitDBtables(String unit_status){//OK
		
    try {
    	
        	
    	if(unit_status.equals("apartment")){
    		Statement st1=DBHelper.getConnection().createStatement();
    	String createTableApartment="CREATE TABLE IF NOT EXISTS apartment(id varchar(32), addressID varchar(32), street varchar(32), city varchar(32), state varchar(2), zip varchar(5))";
    	int custRS1 = st1.executeUpdate(createTableApartment);
    
    	System.out.println("\nPersonDAO3: *************** Query " + createTableApartment);
    	//custRS1.close();   	
    	 //st1.close();
    	}
    	if(unit_status.equals("townhouse")){
    		Statement st1=DBHelper.getConnection().createStatement();	    	
	    	String createTableTownhouse="CREATE TABLE IF NOT EXISTS townhouse(id varchar(32), addressID varchar(32), street varchar(32), city varchar(32), state varchar(2), zip varchar(5))";
	    	int custRS2 = st1.executeUpdate(createTableTownhouse);
	    	System.out.println("\nPersonDAO3: *************** Query " + createTableTownhouse);
    		                    	      //close to manage resources
        	      //custRS2.close();	
	    		//st1.close();
    	}
    	
    	if(unit_status.equals("office")){
    		Statement st1=DBHelper.getConnection().createStatement();
    	String createTableOffice="CREATE TABLE IF NOT EXISTS office(id varchar(32), addressID varchar(32), street varchar(32), city varchar(32), state varchar(2), zip varchar(5))";
    	int custRS3 = st1.executeUpdate(createTableOffice);
    	System.out.println("\nPersonDAO3: *************** Query " + createTableOffice);
    	
   	    //  custRS3.close();
         	st1.close();     
    	}
      } catch (SQLException sql_e) {
      System.err.println("PersonDAO3-create-Unit-tables: Threw a SQLException retrieving the object=units==PDAO3.");
      System.err.println(sql_e.getMessage());
      sql_e.printStackTrace();
    }

}

	public void addUNITStoDB(Unit pers, String unit_status){
			
		//	Person o=new Owner();
		//	Person c=new Consumer();
		//	Person i=new Inspector();
			
			Connection con = DBHelper.getConnection();
	        PreparedStatement personPst = null;
	       // PreparedStatement personPst2 = null;
	      //  PreparedStatement personPst3 = null;

	        try {
	     
	        	
	        	//Insert the Owner object	
	        	 
	        	 if (unit_status=="apartment"){
	           	  System.out.println("TEST insert into APARTMENT: "+pers.getAddressId());
	        		  String personStm = " INSERT INTO apartment(id, addressID , street, city, state, zip ) VALUES(?,?,?,?,?,?)";
	                  personPst = con.prepareStatement(personStm);
	                  personPst.setString(1, pers.getID());
	                  personPst.setString(2, pers.getAddressId());       
	                  personPst.setString(3, pers.getStreet());
	                  personPst.setString(4,pers.getCity());
	                  personPst.setString(5, pers.getState());
	                  personPst.setString(6, pers.getZip());
	                  personPst.executeUpdate();
	                  
	                  System.out.println("\nInsrt ::::Apartment object:   ID:" + pers.getID()+"\n: "+pers.getAddressId()+" "+ pers.getStreet() +" "+pers.getCity()+ ","+" "+pers.getZip()+"\n\n");
	                  
	             }	
	        	
	        	  
	        	  
	        	  //Insert the Townhouse object
	        if(unit_status=="townhouse"){//(status, fname, lname, id)
	    	   
	    	   String personStm2 = " INSERT INTO townhouse(id, addressID, street, city, state, zip) VALUES(?,?,?,?,?,?)";
	    	   personPst = con.prepareStatement(personStm2);
               personPst.setString(1, pers.getID());
               personPst.setString(2, pers.getAddressId());       
               personPst.setString(3, pers.getStreet());
               personPst.setString(4,pers.getCity());
               personPst.setString(5, pers.getState());
               personPst.setString(6, pers.getZip());
               personPst.executeUpdate(); 
               System.out.println("\nInsert the Townhouse object:   ID:" + pers.getID()+"\n: "+pers.getAddressId()+" "+ pers.getStreet() +" "+pers.getCity()+ ","+" "+pers.getZip()+"\n\n");
         	           
       }
	       //Insert the Office object
	         if (unit_status=="office"){
	    	       	
	    	   String personStm3 = " INSERT INTO office(id, addressID, street, city, state, zip) VALUES(?,?,?,?,?,?)";
	    	   personPst = con.prepareStatement(personStm3);
               personPst.setString(1, pers.getID());
               personPst.setString(2, pers.getAddressId());       
               personPst.setString(3, pers.getStreet());
               personPst.setString(4,pers.getCity());
               personPst.setString(5, pers.getState());
               personPst.setString(6, pers.getZip());
               personPst.executeUpdate();
               System.out.println("\nInsert the ::::Office object:   ID:" + pers.getID()+"\n: "+pers.getAddressId()+" "+ pers.getStreet() +" "+pers.getCity()+ ","+" "+pers.getZip()+"\n\n");
         	  
	       }
	       
	                   
	        } catch (SQLException ex) {

	        } finally {

	            try {
	                if (personPst != null) {
	                //	addPst.close();
	                	personPst.close();
	                }
	                if (con != null) {
	                    con.close();
	                }

	            } catch (SQLException ex) {
	      	      System.err.println("PersonsDAO3-add-unit-info-DB: Threw a SQLException saving the person object.");
	    	      System.err.println(ex.getMessage());
	            }
	          
	        }
			
		}
			
	public void createFMaintenaceRequestImplDBtables() throws SQLException{//OK
		// Connection con=DBHelper.getConnection();
	    	 	
		Statement st1=DBHelper.getConnection().createStatement();
////Creates the Consumer, Owner, and Inspector tables in your PostgresSQL database    :))   IT WORKS!! !!
	
    try {
    	
    	String createTableFMReqImp="CREATE TABLE IF NOT EXISTS fMaintenanceRequestImpl(id varchar(32), maintenance_request_type varchar(32), fm_request_complete_or_not varchar(32))";
    	int custRS1 = st1.executeUpdate(createTableFMReqImp);
    
    	System.out.println("\nPersonDAO3: *************** Query " + createTableFMReqImp);
       			    	
	    	    		                    	    
	    		st1.close();
    	        	    	    }
    catch (SQLException sql_e) {
      System.err.println("PersonDAO3-create-PDBFMRImp-tables: Threw a SQLException retrieving the object===PDAO3.");
      System.err.println(sql_e.getMessage());
      sql_e.printStackTrace();
    }
    
    }
	
	
	public void createFMaintenanceCalcImplDBtables() throws SQLException{//OK
		// Connection con=DBHelper.getConnection();
	    	 	
		Statement st1=DBHelper.getConnection().createStatement();

    try {
    	   	String createTableFMCalcImpl="CREATE TABLE IF NOT EXISTS fMaintenanceCalcImpl(id varchar(32),fm_problem_name varchar(32), problem_Rate_for_facility varchar(32), maintenance_cost_for_facility varchar(32), down_time_for_facility varchar(32), labor_cost varchar(32), hours_worked varchar(32), parts_cost varchar(32), number_of_maint_reqsts varchar(32), maintenance_time_hours varchar(32))";
	    	int custRS2 = st1.executeUpdate(createTableFMCalcImpl);
	    	System.out.println("\nPersonDAO3: *************** Query " + createTableFMCalcImpl);                 	    
	    		st1.close();
    	        	    	    }
    catch (SQLException sql_e) {
      System.err.println("PersonDAO3-create-PDB-tables: Threw a SQLException retrieving the object===PDAO3.");
      System.err.println(sql_e.getMessage());
      sql_e.printStackTrace();
    }
    }

	
	public void addEntryIntoFMaintanenceRequestImplDBtable(FMaintenanceRequestImpl fmri){
	Connection con = DBHelper.getConnection();
    PreparedStatement personPst = null;
     try {
     	
    	//Insert the fMaintenanceRequestImpl object	
    	    	      	 
    		  String personStm = "INSERT INTO fMaintenanceRequestImpl(id, maintenance_request_type, fm_request_complete_or_not) values(?,?,?)";
              personPst = con.prepareStatement(personStm);
              personPst.setString(1, fmri.getID());
              personPst.setString(2, fmri.getMaintenanceRequestType());       
              personPst.setString(3, fmri.getFmRequestCompleteOrNot());
              personPst.executeUpdate();
              
              System.out.println("\nInserted ::::fMaintenanceRequestImpl object:   ID:" + fmri.getID()+"\n: "+fmri.getMaintenanceRequestType()+" "+ fmri.getFmRequestCompleteOrNot()+"\n\n");
              
         }catch (SQLException ex) {

	        } finally {

	            try {
	                if (personPst != null) {
	                //	addPst.close();
	                	personPst.close();
	                }
	                if (con != null) {
	                    con.close();
	                }

	            } catch (SQLException ex) {
	      	      System.err.println("PersonsDAO3-add-unit-info-DB: Threw a SQLException saving the person object.");
	    	      System.err.println(ex.getMessage());
	            }
	          
	        }
			
    }
	
	
	public void addEntryIntoFMaintenaceCalcImplDBtable(FMaintenanceCalcImpl fmric){
		Connection con = DBHelper.getConnection();
	    PreparedStatement personPst = null;
	 

	    try {	//Insert the fMaintenanceRequestImpl object	
	    	 
	    	//CREATE TABLE IF NOT EXISTS fMaintenanceCalcImpl(id ,fm_problem_name, problem_Rate_for_facility, maintenance_cost_for_facility, down_time_for_facility) 
	    		
	    		  String personStm = " INSERT INTO fMaintenanceCalcImpl(id ,fm_problem_name, problem_Rate_for_facility, " +
	    		  		"maintenance_cost_for_facility, " +
	    		  		"down_time_for_facility," +
	    		  		"labor_cost,hours_worked, parts_cost, number_of_maint_reqsts, maintenance_time_hours) values(?,?,?,?,?,?,?,?,?,?)";
	  	    	
	              personPst = con.prepareStatement(personStm);
	              personPst.setString(1, fmric.getID());
	              personPst.setString(2, fmric.getFmProblemName());       
	              personPst.setString(3, fmric.getProblemRateForFacility());
	              personPst.setString(4, fmric.getMaintCostForFasility());
	              personPst.setString(5, fmric.getDownTimeForFacility());
	              personPst.setString(6, fmric.getLaborCost());
          		  personPst.setString(7, fmric.getHoursWorked());
   				  personPst.setString(8, fmric.getPartsCost());
				  personPst.setString(9, fmric.getNumberOfMaintRequest());
	              personPst.setString(10, fmric.getMaintTimeHours());
	              personPst.executeUpdate();
	              
	              System.out.println("\nInserted ::::fMaintenanceCalcImpl object:   ID:" + fmric.getID()+"\n: "+fmric.getFmProblemName()+" "+ fmric.getProblemRateForFacility()+" "+fmric.getMaintCostForFasility()+" "+fmric.getDownTimeForFacility()+"\n\n");
	              
	         } 	 
	    	 catch (SQLException ex) {

		        } finally {

		            try {
		                if (personPst != null) {
		                //	addPst.close();
		                	personPst.close();
		                }
		                if (con != null) {
		                    con.close();
		                }

		            } catch (SQLException ex) {
		      	      System.err.println("PersonsDAO3-add-unit-info-DB: Threw a SQLException saving the person object.");
		    	      System.err.println(ex.getMessage());
		            }
		          
		        }
				
	    }	
	
	
	
	
    public void createFUseListAndCalcDBtables() throws SQLException{//OK
		// Connection con=DBHelper.getConnection();
	    	 	
		Statement st2=DBHelper.getConnection().createStatement();  ////Creates FacilityUse table in your PostgresSQL database    :))   IT WORKS!! !!
	
    try {
    	
    	String createTableFUlist="CREATE TABLE IF NOT EXISTS facilityUse(ID varchar(32), is_in_use varchar(32), assigned_to_use varchar(32), is_vacated varchar(32), facility_inspection_name varchar(32), usage_rate varchar(32))";
    	int custRS1 = st2.executeUpdate(createTableFUlist);
    
    	System.out.println("\nPersonDAO3: *************** Query " + createTableFUlist);
         } catch (SQLException sql_e) {
      System.err.println("PersonDAO3-create-FU-tables: Threw a SQLException retrieving the object===PDAO3.");
      System.err.println(sql_e.getMessage());
      sql_e.printStackTrace();
    }
    
}

    
    public void addEntryIntoFUlistandCalcDBtables(FacilityUse fu){
    	Connection con = DBHelper.getConnection();
        PreparedStatement personPst = null;
   	 
         try {
        	 
        	//Insert the FacilityUse object	
        	 System.out.println("JUMPED to TRY()");  
        	 
 String personStm = "INSERT INTO facilityUse(ID, is_in_use, assigned_to_use, is_vacated , facility_inspection_name, usage_rate) values(?,?,?,?,?,?)";
                  personPst = con.prepareStatement(personStm);
                  personPst.setString(1, fu.getID());
                  personPst.setString(2, fu.getIsInUse());       
                  personPst.setString(3, fu.getAssignedToUse());
                  personPst.setString(4, fu.getIsVacated());
                  personPst.setString(5, fu.getFacilityInspectionName());
                  personPst.setString(6, fu.getUsageRate());
                  personPst.executeUpdate();
                  
                  System.out.println("\nInserted ::::FUse object:   ID:" + fu.getID()+" "+fu.getIsInUse()+" "+ fu.getAssignedToUse()+" "+fu.getIsVacated()+" "+fu.getFacilityInspectionName()+" "+fu.getUsageRate()+"\n\n");
                  
             }catch (SQLException ex) {

    	        } finally {

    	            try {
    	                if (personPst != null) {
    	                //	addPst.close();
    	                	personPst.close();
    	                }
    	                if (con != null) {
    	                    con.close();
    	                }

    	            } catch (SQLException ex) {
    	      	      System.err.println("PersonsDAO3-add-Fuse-info-DB: Threw a SQLException saving the person object.");
    	    	      System.err.println(ex.getMessage());
    	            }
    	          
    	        }
    			
        }
    	
    

}