
package edu.luc.facilityRoot.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.luc.facilityRoot.model.facilityMaintainance.FMaintenanceRequestImpl;
import edu.luc.facilityRoot.dao.HibernatePGSQLHelper;
import edu.luc.facilityRoot.*;


public class FMaintenanceRequestImplHibernateDAO {

	public void addFMaintenanceRequestImpl(FMaintenanceRequestImpl cust) {
		System.out.println("*************** Adding FMaintenanceRequest information in DB with ID ...  " + cust.getID());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(cust);
		session.getTransaction().commit();
	}
	
	public void deleteFMaintenanceRequestImpl(FMaintenanceRequestImpl cust) {
		System.out.println("*************** Deleteing FMaintenanceRequest information in DB with ID ...  " + cust.getID());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(cust);
		session.getTransaction().commit();
	}
	
	public FMaintenanceRequestImpl retrieveFMaintenanceRequestImpl(String ID) {
		try {
		System.out.println("*************** Searcing for FMaintenanceRequest information with ID ...  " + ID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		//System.out.println("*************** Hibernate session is created ..................\n" + session.toString());
		
		//Query getCustQuery = session.createQuery("From CustomerImpl ");
		Query getFMRQuery = session.createQuery("From FMaintenanceRequestImpl where ID=:ID");		
		getFMRQuery.setString("ID", ID);
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getFMRQuery.toString()); 
		
		List fmRequestsInfo = getFMRQuery.list();
		System.out.println("Getting FMaintenanceRequest Details using HQL. \n" + fmRequestsInfo);

		session.getTransaction().commit();
		return (FMaintenanceRequestImpl)fmRequestsInfo.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public FMaintenanceRequestImpl retrieveMaintenaceRequestInfo(String ID) {
		try {
		System.out.println("*************** Searcing for facilirty maintenace request information with ID ...  " + ID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
               
        Query getFMRinfoQuery = session.createQuery("From FMaintenanceRequestImpl where ID=:ID");		
        getFMRinfoQuery.setString("ID", ID);
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getFMRinfoQuery.toString()); 
		
		List fmRequestsInfo = getFMRinfoQuery.list();
		System.out.println("Getting Fasility Maintenance Request info using HQL. \n" + fmRequestsInfo.get(0));
		
		System.out.println("*************** Retrieve Query is ....>>\n" + fmRequestsInfo.get(0).toString()); 
		
		session.getTransaction().commit();
		return (FMaintenanceRequestImpl)fmRequestsInfo.get(0);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
