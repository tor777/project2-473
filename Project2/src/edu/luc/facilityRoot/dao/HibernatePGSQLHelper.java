package edu.luc.facilityRoot.dao;




import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernatePGSQLHelper {
	private static final SessionFactory sessionFactory;

	static {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new Configuration().configure().buildSessionFactory();
			System.out.println("*************** Session Factory instantiated ..................");
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}








/*
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

//import org.hibernate.HibernateException;
//import org.hibernate.SessionFactory;
//import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
//import org.hibernate.service.ServiceRegistryBuilder;


public class HibernatePGSQLHelper {

	private static final SessionFactory sessionFactory;

	static {
		try {
		
		
		 Configuration configuration = new Configuration();

         configuration.configure();

          ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(

         configuration.getProperties()).build();

         sessionFactory = new Configuration().configure().buildSessionFactory(serviceRegistry);
				
			// Create the SessionFactory from hibernate.cfg.xml
		//	sessionFactory = new Configuration().configure().buildSessionFactory(); DEPRICATE in Hibernate 4
			System.out.println("*************** Session Factory instantiated ..................");
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
}
	*/