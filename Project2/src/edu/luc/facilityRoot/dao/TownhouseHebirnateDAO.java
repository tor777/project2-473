package edu.luc.facilityRoot.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.luc.facilityRoot.model.facility.Townhouse;
import edu.luc.facilityRoot.model.facility.Unit;
import edu.luc.facilityRoot.model.persons.*;
import edu.luc.facilityRoot.dao.HibernatePGSQLHelper;
import edu.luc.facilityRoot.*;


public class TownhouseHebirnateDAO {

	public void addTownhouse(Unit townhouse) {
		System.out.println("*************** Adding unit information in DB with ID ...  " + townhouse.getID());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(townhouse);
		session.getTransaction().commit();
	}
	
	public void deleteTownhouse(Unit unit) {
		System.out.println("*************** Deleteing unit information in DB with ID ...  " + unit.getID());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(unit);
		session.getTransaction().commit();
	}
	
	public Townhouse retrieveTownhouse(String ID) {
		try {
		System.out.println("*************** Searcing for unit information with ID ...  " + ID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	    
		Query getInspQuery = session.createQuery("From Townhouse where ID=:ID");		
		getInspQuery.setString("ID", ID);
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getInspQuery.toString()); 
		
		List UnitInfo = getInspQuery.list();
		System.out.println("Getting Townhouse Details using HQL. \n" + UnitInfo);

		session.getTransaction().commit();
		return (Townhouse)UnitInfo.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/*
	public Townhouse retrieveTownhouse(String ID) {
		try {
		System.out.println("*************** Searcing for townhouse information with ID ...  " + ID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	               
        Query getTownhouseQuery = session.createQuery("From Townhouse where ID=:ID");		
        getTownhouseQuery.setString("ID", ID);
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getITownhouseQuery.toString()); 
		
		List TownhouseInfo = getTownhouseQuery.list();
		System.out.println("Getting townhouse info using HQL. \n" + TownhouseInfo.get(0));
		
		System.out.println("*************** Retrieve Query is ....>>\n" + Townhouse.get(0).toString()); 
		
		session.getTransaction().commit();
		return (Townhouse)TownhouseInfo.get(0);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	*/
}
