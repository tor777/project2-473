package edu.luc.facilityRoot.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import edu.luc.facilityRoot.model.persons.Inspector;
import edu.luc.facilityRoot.model.persons.*;
import edu.luc.facilityRoot.dao.HibernatePGSQLHelper;
import edu.luc.facilityRoot.*;


public class InspectorHebirnateDAO {

	public void addInspector(Inspector inspector) {
		System.out.println("*************** Adding inspector information in DB with ID ...  " + inspector.getId());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(inspector);
		session.getTransaction().commit();
	}
	
	public void deleteInspector(Inspector inspector) {
		System.out.println("*************** Deleteing inspector information in DB with ID ...  " + inspector.getId());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(inspector);
		session.getTransaction().commit();
	}
	
	public Inspector retrieveInspector(String ID) {
		try {
		System.out.println("*************** Searcing for inspector information with ID ...  " + ID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	    
		Query getInspQuery = session.createQuery("From Inspector where ID=:ID");		
		getInspQuery.setString("ID", ID);
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getInspQuery.toString()); 
		
		List InspectorInfo = getInspQuery.list();
		System.out.println("Getting Inspector Details using HQL. \n" + InspectorInfo);

		session.getTransaction().commit();
		return (Inspector)InspectorInfo.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
}
